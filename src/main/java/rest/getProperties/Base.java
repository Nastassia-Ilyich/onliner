package rest.getProperties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Base {

    private static final Properties base;

    static {
        base = new Properties();
        InputStream is = Base.class.getResourceAsStream("/base/base.properties");

        try {
            base.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getValue(String variable) {
        return base.getProperty(variable);
    }
}
