package rest.searchResults;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import rest.getProperties.Base;


public class Specifications {
    public static final RequestSpecification requestSpec = new RequestSpecBuilder()
            .setBaseUri(Base.getValue("BasePage.baseURL"))
            .setAccept(ContentType.JSON)
            .setContentType(ContentType.JSON)
            .log(LogDetail.ALL)
            .build();
}
