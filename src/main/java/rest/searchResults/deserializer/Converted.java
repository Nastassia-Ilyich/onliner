
package rest.searchResults.deserializer;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "BYN",
    "BYR"
})
public class Converted {

    @JsonProperty("BYN")
    private BYN bYN;
    @JsonProperty("BYR")
    private BYR bYR;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("BYN")
    public BYN getBYN() {
        return bYN;
    }

    @JsonProperty("BYN")
    public void setBYN(BYN bYN) {
        this.bYN = bYN;
    }

    @JsonProperty("BYR")
    public BYR getBYR() {
        return bYR;
    }

    @JsonProperty("BYR")
    public void setBYR(BYR bYR) {
        this.bYR = bYR;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("bYN", bYN).append("bYR", bYR).append("additionalProperties", additionalProperties).toString();
    }

}
