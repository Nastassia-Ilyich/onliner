
package rest.searchResults.deserializer;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "min",
    "price_min",
    "max",
    "price_max",
    "currency_sign",
    "offers",
    "html_url",
    "url"
})
public class Prices {

    @JsonProperty("min")
    private Object min;
    @JsonProperty("price_min")
    private PriceMin priceMin;
    @JsonProperty("max")
    private Object max;
    @JsonProperty("price_max")
    private PriceMax priceMax;
    @JsonProperty("currency_sign")
    private Object currencySign;
    @JsonProperty("offers")
    private Offers offers;
    @JsonProperty("html_url")
    private String htmlUrl;
    @JsonProperty("url")
    private String url;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("min")
    public Object getMin() {
        return min;
    }

    @JsonProperty("min")
    public void setMin(Object min) {
        this.min = min;
    }

    @JsonProperty("price_min")
    public PriceMin getPriceMin() {
        return priceMin;
    }

    @JsonProperty("price_min")
    public void setPriceMin(PriceMin priceMin) {
        this.priceMin = priceMin;
    }

    @JsonProperty("max")
    public Object getMax() {
        return max;
    }

    @JsonProperty("max")
    public void setMax(Object max) {
        this.max = max;
    }

    @JsonProperty("price_max")
    public PriceMax getPriceMax() {
        return priceMax;
    }

    @JsonProperty("price_max")
    public void setPriceMax(PriceMax priceMax) {
        this.priceMax = priceMax;
    }

    @JsonProperty("currency_sign")
    public Object getCurrencySign() {
        return currencySign;
    }

    @JsonProperty("currency_sign")
    public void setCurrencySign(Object currencySign) {
        this.currencySign = currencySign;
    }

    @JsonProperty("offers")
    public Offers getOffers() {
        return offers;
    }

    @JsonProperty("offers")
    public void setOffers(Offers offers) {
        this.offers = offers;
    }

    @JsonProperty("html_url")
    public String getHtmlUrl() {
        return htmlUrl;
    }

    @JsonProperty("html_url")
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("min", min).append("priceMin", priceMin).append("max", max).append("priceMax", priceMax).append("currencySign", currencySign).append("offers", offers).append("htmlUrl", htmlUrl).append("url", url).append("additionalProperties", additionalProperties).toString();
    }

}
