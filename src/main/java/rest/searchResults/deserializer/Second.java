
package rest.searchResults.deserializer;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "offers_count",
    "min_price"
})
public class Second {

    @JsonProperty("offers_count")
    private Integer offersCount;
    @JsonProperty("min_price")
    private Object minPrice;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("offers_count")
    public Integer getOffersCount() {
        return offersCount;
    }

    @JsonProperty("offers_count")
    public void setOffersCount(Integer offersCount) {
        this.offersCount = offersCount;
    }

    @JsonProperty("min_price")
    public Object getMinPrice() {
        return minPrice;
    }

    @JsonProperty("min_price")
    public void setMinPrice(Object minPrice) {
        this.minPrice = minPrice;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("offersCount", offersCount).append("minPrice", minPrice).append("additionalProperties", additionalProperties).toString();
    }

}
