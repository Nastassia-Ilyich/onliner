
package rest.searchResults.deserializer;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "key",
    "name",
    "full_name",
    "name_prefix",
    "extended_name",
    "schema",
    "status",
    "images",
    "description",
    "html_url",
    "reviews",
    "review_url",
    "url",
    "prices",
    "second"
})
public class Product {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("key")
    private String key;
    @JsonProperty("name")
    private String name;
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("name_prefix")
    private String namePrefix;
    @JsonProperty("extended_name")
    private String extendedName;
    @JsonProperty("schema")
    private Schema schema;
    @JsonProperty("status")
    private String status;
    @JsonProperty("images")
    private Images images;
    @JsonProperty("description")
    private String description;
    @JsonProperty("html_url")
    private String htmlUrl;
    @JsonProperty("reviews")
    private Reviews reviews;
    @JsonProperty("review_url")
    private String reviewUrl;
    @JsonProperty("url")
    private String url;
    @JsonProperty("prices")
    private Prices prices;
    @JsonProperty("second")
    private Second second;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("full_name")
    public String getFullName() {
        return fullName;
    }

    @JsonProperty("full_name")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @JsonProperty("name_prefix")
    public String getNamePrefix() {
        return namePrefix;
    }

    @JsonProperty("name_prefix")
    public void setNamePrefix(String namePrefix) {
        this.namePrefix = namePrefix;
    }

    @JsonProperty("extended_name")
    public String getExtendedName() {
        return extendedName;
    }

    @JsonProperty("extended_name")
    public void setExtendedName(String extendedName) {
        this.extendedName = extendedName;
    }

    @JsonProperty("schema")
    public Schema getSchema() {
        return schema;
    }

    @JsonProperty("schema")
    public void setSchema(Schema schema) {
        this.schema = schema;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("images")
    public Images getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(Images images) {
        this.images = images;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("html_url")
    public String getHtmlUrl() {
        return htmlUrl;
    }

    @JsonProperty("html_url")
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    @JsonProperty("reviews")
    public Reviews getReviews() {
        return reviews;
    }

    @JsonProperty("reviews")
    public void setReviews(Reviews reviews) {
        this.reviews = reviews;
    }

    @JsonProperty("review_url")
    public String getReviewUrl() {
        return reviewUrl;
    }

    @JsonProperty("review_url")
    public void setReviewUrl(String reviewUrl) {
        this.reviewUrl = reviewUrl;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("prices")
    public Prices getPrices() {
        return prices;
    }

    @JsonProperty("prices")
    public void setPrices(Prices prices) {
        this.prices = prices;
    }

    @JsonProperty("second")
    public Second getSecond() {
        return second;
    }

    @JsonProperty("second")
    public void setSecond(Second second) {
        this.second = second;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("key", key).append("name", name).append("fullName", fullName).append("namePrefix", namePrefix).append("extendedName", extendedName).append("schema", schema).append("status", status).append("images", images).append("description", description).append("htmlUrl", htmlUrl).append("reviews", reviews).append("reviewUrl", reviewUrl).append("url", url).append("prices", prices).append("second", second).append("additionalProperties", additionalProperties).toString();
    }

}
