package pageObjects;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ProductDetailsPage extends BasePage {
    private final SelenideElement productTitleOnPDP = $(By.className("catalog-masthead__title"));
    private final SelenideElement offersDescription = $(By.className("offers-description"));
    private final SelenideElement fotorama = $(By.cssSelector(".fotorama__stage__shaft"));
    private final ElementsCollection pictureThumbs = $$(By.className("product-gallery__thumb-img"));
    private final ElementsCollection scrollBoxThumbs = $$(By.cssSelector("div#mCSB_1_container > div > div"));
    private final SelenideElement fotoramaBackPointer = $(By.className("fotorama__arr--prev"));
    private final SelenideElement fotoramaNextPointer = $(By.className("fotorama__arr--next"));
    private final SelenideElement addToCardShopRatedButton = $(By.cssSelector(".product-aside__item.product-aside__item--byn.product-aside__item--highlighted.state_add-to-cart .button.button_orange.product-aside__item-button"));
    private final SelenideElement contactsShopRatedButton = $(By.cssSelector(".product-aside__item.product-aside__item--byn.product-aside__item--highlighted.state_add-to-cart .b-offers-desc__select__trigger.button.button_middle.button_white.offers-list__button.offers-list__button_contacts.pseudolink.pseudolink_unstyled"));
    private final SelenideElement topRatedShop = $(By.cssSelector(".product-aside__item.product-aside__item--byn.product-aside__item--highlighted.state_add-to-cart"));
    private final SelenideElement notAuthorisedCart = $(By.cssSelector(".auth-bar__item--cart"));
    private final SelenideElement notAuthorisedCartIndicator = $(By.cssSelector(".auth-bar__counter"));
    private final SelenideElement profileCartIndicator = $(By.cssSelector(".b-top-profile__list > div#cart-desktop > .b-top-profile__cart > .b-top-profile__counter"));
    private final SelenideElement profileCart = $(By.cssSelector(".b-top-profile__list > div#cart-desktop > .b-top-profile__cart"));
    private final SelenideElement checkedAddButton = $(By.cssSelector(".button.button_orange.product-aside__item-button.product-aside__item-button_checked"));

    public SelenideElement getCheckedAddButton() {
        return checkedAddButton;
    }

    public SelenideElement getNotAuthorisedCart() {
        return notAuthorisedCart;
    }

    public SelenideElement getNotAuthorisedCartIndicator() {
        return notAuthorisedCartIndicator;
    }


    public SelenideElement getProfileCartIndicator() {
        return profileCartIndicator;
    }

    public SelenideElement getProfileCart() {
        return profileCart;
    }

    public SelenideElement getTopRatedShop() {
        return topRatedShop;
    }
    public SelenideElement getContactsShopRatedButton() {
        return contactsShopRatedButton;
    }

    public SelenideElement getAddToCardShopRatedButton() {
        return addToCardShopRatedButton;
    }

    public SelenideElement getFotoramaBackPointer() {
        return fotoramaBackPointer;
    }

    public SelenideElement getFotoramaNextPointer() {
        return fotoramaNextPointer;
    }

    public ElementsCollection getScrollBoxThumbs() {
        return scrollBoxThumbs;
    }

    public SelenideElement getFotorama() {
        return fotorama;
    }

    public SelenideElement getOffersDescription() {
        return offersDescription;
    }

    public ElementsCollection getPictureThumbs() {
        return pictureThumbs;
    }

    public SelenideElement getProductTitleOnPDP() {
        return productTitleOnPDP;
    }

}
