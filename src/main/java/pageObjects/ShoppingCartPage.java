package pageObjects;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ShoppingCartPage extends BasePage {
    private final ElementsCollection addedItems = $$(By.className("cart-form__offers-part_data"));
    private final ElementsCollection addedItemsTitlesCollection = $$(By.cssSelector(".cart-form__description_font-weight_semibold.cart-form__description_condensed-other > a"));
    private final ElementsCollection trashButtonsInCarts = $$(By.cssSelector(".cart-form__offers-part_remove > .cart-form__control"));
    private final ElementsCollection deletedItems = $$(By.linkText("Вернуть товар"));
    private final By addedItemTitle = By.cssSelector(".cart-form__description_font-weight_semibold.cart-form__description_condensed-other > a");
    private final ElementsCollection deletedItemTitle = $$(By.cssSelector(".cart-form__description_base-alter.cart-form__description_condensed-extra"));
    private final SelenideElement cartContainer = $(By.cssSelector(".cart-form__body > .cart-container.cart-container_max-width_xxxxxl"));
    private final SelenideElement cartIsEmpty = $(By.cssSelector("div.cart-message__title.cart-message__title_big"));

    public SelenideElement getCartIsEmpty() {
        return cartIsEmpty;
    }

    public SelenideElement getCartContainer() {
        return cartContainer;
    }

    public ElementsCollection getAddedItemsTitlesCollection() {
        return addedItemsTitlesCollection;
    }

    public ElementsCollection getDeletedItemTitle() {
        return deletedItemTitle;
    }

    public By getAddedItemTitle() {
        return addedItemTitle;
    }


    public ElementsCollection getDeletedItems() {
        return deletedItems;
    }


    public ElementsCollection getTrashButtonsInCarts() {
        return trashButtonsInCarts;
    }


    public ElementsCollection getAddedItems() {
        return addedItems;
    }
}
