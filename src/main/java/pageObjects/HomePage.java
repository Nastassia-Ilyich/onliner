package pageObjects;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class HomePage extends BasePage {
    private final SelenideElement enterBtn = $(By.cssSelector(".auth-bar__item.auth-bar__item--text"));
    private final SelenideElement userName = $x("//input[@placeholder='Ник или e-mail']");
    private final SelenideElement userPassword = $x("//input[@placeholder='Пароль']");
    private final SelenideElement loginBtn = $(By.cssSelector(".auth-button.auth-button_middle.auth-button_primary.auth-form__button.auth-form__button_width_full"));
    private final SelenideElement profileImage =  $(By.cssSelector(".b-top-profile__image.js-header-user-avatar"));
    private final SelenideElement logoutBtn =  $(By.cssSelector(".b-top-profile__logout > .b-top-profile__link.b-top-profile__link_secondary"));
    private final SelenideElement searchInputField = $(By.cssSelector("input[name='query']"));
    private final SelenideElement searchInputFieldInModalWindow = $(By.cssSelector(".search__input"));
    private final SelenideElement header = $x("//header/div[3]");
    private final ElementsCollection searchResults = $$(By.cssSelector(".result__item.result__item_product"));
    private final SelenideElement searchIFrame = $x("//iframe[@class='modal-iframe']");
    private final SelenideElement productInTheQuickSearchResultsList = $(By.cssSelector("li:nth-of-type(1) .product__title-link"));
    private final SelenideElement topActions = $(By.cssSelector(".b-top-actions"));

    public SelenideElement getTopActions() {
        return topActions;
    }


    public SelenideElement getHeader() {
        return header;
    }

    public SelenideElement getProductInTheQuickSearchResultsList() {
        return productInTheQuickSearchResultsList;
    }

    public SelenideElement getSearchInputFieldInModalWindow() {
        return searchInputFieldInModalWindow;
    }

    public ElementsCollection getSearchResults() {
        return searchResults;
    }

    public SelenideElement getSearchIFrame() {
        return searchIFrame;
    }

    public SelenideElement getEnterBtn() {
        return enterBtn;
    }

    public SelenideElement getUserName() {
        return userName;
    }

    public SelenideElement getUserPassword() {
        return userPassword;
    }

    public SelenideElement getLoginBtn() {
        return loginBtn;
    }

    public SelenideElement getProfileImage() {
        return profileImage;
    }

    public SelenideElement getLogoutBtn() {
        return logoutBtn;
    }

    public SelenideElement getSearchInputField() {
        return searchInputField;
    }

}
