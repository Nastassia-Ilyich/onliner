package pageObjects;

import org.openqa.selenium.WebDriver;
import rest.getProperties.Base;

public class BasePage {
 public WebDriver driver;
 public String baseURL = Base.getValue(this.getClass().getSimpleName()+ ".baseURL");
}
