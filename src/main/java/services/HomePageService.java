package services;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import lombok.extern.log4j.Log4j;
import org.junit.Assert;
import pageObjects.HomePage;
import rest.getProperties.Base;
import rest.searchResults.SearchResults;
import rest.searchResults.deserializer.Product;
import state.World;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.WebDriverRunner.*;
import static org.junit.Assert.assertTrue;
import static rest.searchResults.Specifications.requestSpec;

@Log4j
public class HomePageService {
    private final HomePage page;

    public HomePageService() {
        this.page = new HomePage();
    }

    public HomePageService clickEnter() {
        page.getEnterBtn().should(Condition.exist).click();
        return this;
    }

    public HomePageService setUserName(String name) {
        page.getUserName().should(Condition.exist).setValue(name);
        return this;
    }

    public HomePageService setPassword(String password) {
        page.getUserPassword().should(Condition.exist).sendKeys(password);
        return this;
    }

    public HomePageService clickLogin() {
        page.getLoginBtn().click();
        return this;
    }

    public HomePageService checkProfileImageAppears() {
        page.getProfileImage().shouldBe(Condition.exist);
        return this;
    }

    public HomePageService login(String name, String password) {
        page.getEnterBtn().waitUntil(Condition.exist, 10000).click();
        page.getUserName().should(Condition.exist).setValue(name);
        page.getUserPassword().should(Condition.exist).sendKeys(password);
        page.getLoginBtn().click();
        log.info("User is logged in");
        return this;
    }

    public HomePageService clickProfileImage() {
        World world = new World();
        world.setCurrentUrl(getWebDriver().getCurrentUrl());
        page.getProfileImage().should(Condition.exist).click();
        return this;
    }

    public HomePageService clickLogout() {
        page.getLogoutBtn().should(Condition.exist).click();
        return this;
    }

    public HomePageService checkProfileImageDisappear() {
        page.getHeader().waitUntil(Condition.exist, 10000);
        page.getProfileImage().shouldNot(Condition.exist);
        return this;
    }


    public HomePageService checkUserStaysOnTheSamePage(String previouslySavedPage) {
        log.info("Current URL " + url());
        log.info("Previously saved page " + previouslySavedPage);
        assertTrue(url().equals(previouslySavedPage));
        return this;
    }

    public HomePageService clickOnSearchField() {
        page.getSearchInputField().waitUntil(Condition.exist, 10000).should(Condition.exist).click();
        return this;
    }

    public HomePageService enterSearchQuery(String searchQuery) {
        String firstLetter = "";
        StringBuilder allOtherLetters = new StringBuilder();
        boolean loop = true;
        page.getSearchInputField().click();

        while (loop) {
            List<Character> charList = searchQuery.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
            if (!charList.get(0).toString().isEmpty()) {
                firstLetter = charList.get(0).toString();
                for (int i = 1; i < charList.size(); i++) {
                    allOtherLetters.append(charList.get(i).toString());
                    loop = false;
                }
            } else {
                charList.remove(0);
            }
        }
        page.getSearchInputField().sendKeys(firstLetter);
        log.info("First letter is entered in the search field, modal window is opened");
        driver().switchTo().defaultContent();
        driver().switchTo().frame(page.getSearchIFrame());
        page.getSearchInputFieldInModalWindow().sendKeys(allOtherLetters);
        log.info("All the other letters are entered in the search field, modal window is opened");
        page.getSearchResults().get(1).should(Condition.text(searchQuery));
        return this;
    }

    public HomePageService checkSearchResults(String searchQuery) {
        for (SelenideElement element : page.getSearchResults()) {
            log.info("Element have text: " + element.getText());
            element.waitUntil(Condition.exist, 10000).shouldHave(Condition.text(searchQuery));
        }
        return this;
    }

    public HomePageService checkSearchAPIResults(String searchQuery) {
        SearchResults readValues = null;
        ObjectMapper mapper = new ObjectMapper();

        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        String products = RestAssured
                .given(requestSpec)
                .get(Base.getValue("BasePage.baseURL") + Base.getValue("Endpoints.GET_PRODUCTS_BY_QUERY"), searchQuery)
                .then()
                .statusCode(200)
                .extract()
                .asString();
        try {
            readValues = mapper.readValue(products, SearchResults.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (readValues != null) {
            for (Product product : readValues.getProducts()) {
                log.info("Product's extended name is " + product.getExtendedName());
                Assert.assertTrue(product.getExtendedName().contains(searchQuery));
            }
        }
        return this;
    }

    public HomePageService clickOnFirstProductInSearchResults() {
        page.getProductInTheQuickSearchResultsList().click();
        return this;
    }

}
