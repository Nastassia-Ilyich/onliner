package services;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideWait;
import lombok.extern.log4j.Log4j;
import pageObjects.HomePage;
import pageObjects.ProductDetailsPage;
import state.World;

import static com.codeborne.selenide.WebDriverRunner.*;
import static org.junit.Assert.*;

@Log4j
public class ProductDetailsPageService {
    private int actualIndicatorResult;
    private final ProductDetailsPage pdpPage;
    private final HomePage homePage = new HomePage();
    World world = new World();

    public ProductDetailsPageService() {
        this.pdpPage = new ProductDetailsPage();
    }

    public ProductDetailsPageService checkUserIsOnNecessaryPDPPage(String searchQuery) {
        pdpPage.getProductTitleOnPDP().scrollTo();
        log.info("User should be redirected to PDP. Current URL" + url());
        assertTrue(url().contains("catalog") && pdpPage.getProductTitleOnPDP().getText().toLowerCase().contains(searchQuery.toLowerCase()));
        return this;
    }

    public int getQtyOfElementsFromTheScrollBar() {
        pdpPage.getOffersDescription().scrollTo();
        log.info("There is (are) " + pdpPage.getScrollBoxThumbs().size() + " element(s) in the scroll bar");
        return pdpPage.getScrollBoxThumbs().size();
    }

    public ElementsCollection getCollectionOfThumbnailsElementsFromTheScrollBar() {
        pdpPage.getOffersDescription().scrollTo();
        return pdpPage.getScrollBoxThumbs();
    }

    public ProductDetailsPageService clickOnTheFirstThumbnailFromTheScrollBar() {
        pdpPage.getScrollBoxThumbs().first().click();
        world.setCurrentFotoramaElement(pdpPage.getScrollBoxThumbs().first());
        pdpPage.getFotorama().scrollTo();
        log.info("First element from the scroll bar is opened");
        return this;
    }

    public ProductDetailsPageService clicksOnTheLastThumbnail() {
        pdpPage.getScrollBoxThumbs().last().click();
        world.setCurrentFotoramaElement(pdpPage.getScrollBoxThumbs().last());
        pdpPage.getFotorama().scrollTo();
        log.info("Last element from the scroll bar is opened");
        return this;
    }

    public ProductDetailsPageService checkExistenceOfTheFotorama() {
        assertTrue(pdpPage.getFotorama().isDisplayed());
        log.info("Fotorama is displayed");
        return this;
    }

    public ProductDetailsPageService checkThumbnailInFotoramaChangesOnTheSecond() {
        assertNotEquals(World.getPreviouslyFotoramaElement(), pdpPage.getScrollBoxThumbs().get(1));
        log.info("The second element from the scroll bar is opened");
        return this;
    }

    public boolean checkExistenceOfBackPointer() {
        if (pdpPage.getFotoramaBackPointer().hover().isDisplayed()) {
            log.info("Back pointer exists");
            return true;
        } else {
            log.info("Back pointer does not exist");
            return false;
        }
    }

    public boolean checkExistenceOfNextPointer() {
        if (pdpPage.getFotoramaNextPointer().hover().isDisplayed()) {
            log.info("Next pointer exists");
            return true;
        } else {
            log.info("Next pointer does not exist");
            return false;
        }
    }

    public ProductDetailsPageService clickFotoramaNextPointer() {
        pdpPage.getFotoramaNextPointer().hover().click();
        log.info("The next element from the scroll bar is opened");
        return this;
    }

    public ProductDetailsPageService clickFotoramaBackPointer() {
        pdpPage.getFotoramaBackPointer().hover().click();
        log.info("The previous element from the scroll bar is opened");
        return this;
    }

    public ProductDetailsPageService checkThumbnailInFotoramaChangesOnTheLast() {
        log.info("Previously fotorama element was " + World.getPreviouslyFotoramaElement());
        log.info("Actual fotorama element is " + pdpPage.getScrollBoxThumbs().last());
        assertNotEquals(World.getPreviouslyFotoramaElement(), pdpPage.getScrollBoxThumbs().last());
        return this;
    }

    public ProductDetailsPageService checkButtonsExistenceForTopRatedShop() {
        pdpPage.getTopRatedShop().scrollTo();
        assertTrue(pdpPage.getAddToCardShopRatedButton().waitUntil(Condition.exist, 10000).exists() && pdpPage.getContactsShopRatedButton().waitUntil(Condition.exist, 10000).exists());
        return this;
    }

    public ProductDetailsPageService clickOnAddToButtonNearTheRatedShop() {
        getWebDriver().navigate().refresh();
        homePage.getTopActions().waitUntil(Condition.visible, 10000);
        if (homePage.getProfileImage().exists()) {
            if (pdpPage.getProfileCartIndicator().isDisplayed()) {
                actualIndicatorResult = Integer.parseInt(pdpPage.getProfileCartIndicator().getText());
            } else {
                actualIndicatorResult = 0;
            }
        } else {
            if (pdpPage.getNotAuthorisedCartIndicator().isDisplayed()) {
                actualIndicatorResult = Integer.parseInt(pdpPage.getNotAuthorisedCartIndicator().getText());
            } else {
                actualIndicatorResult = 0;
            }
        }
        if (!pdpPage.getCheckedAddButton().exists()) {
            pdpPage.getAddToCardShopRatedButton().waitUntil(Condition.exist, 10000).click();
            actualIndicatorResult++;
        }
        World.setActualIndicatorResult(actualIndicatorResult);
        return this;
    }

    public ProductDetailsPageService goToTheShoppingCart() {
        getWebDriver().navigate().refresh();
        homePage.getTopActions().waitUntil(Condition.visible, 10000);
        if (homePage.getProfileImage().exists()) {
            pdpPage.getProfileCart().waitUntil(Condition.exist, 10000).click();
        } else
            pdpPage.getNotAuthorisedCart().waitUntil(Condition.exist, 10000).click();
        return this;
    }

    public ProductDetailsPageService checkIndicatorIsShown() {
        if (homePage.getProfileImage().exists()) {
            assertEquals(Integer.parseInt(pdpPage.getProfileCartIndicator().waitUntil(Condition.text(String.valueOf(actualIndicatorResult)), 10000).should(Condition.exist).text()), actualIndicatorResult);
        } else
            assertEquals(Integer.parseInt(pdpPage.getNotAuthorisedCartIndicator().waitUntil(Condition.text(String.valueOf(actualIndicatorResult)), 10000).should(Condition.exist).text()), actualIndicatorResult);
        return this;
    }
}
