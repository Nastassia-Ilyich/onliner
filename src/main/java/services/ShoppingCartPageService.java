package services;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lombok.extern.log4j.Log4j;
import pageObjects.ShoppingCartPage;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

@Log4j
public class ShoppingCartPageService {
    private final ShoppingCartPage shoppingCartPage;

    public ShoppingCartPageService() {
        this.shoppingCartPage = new ShoppingCartPage();
    }

    public int getAddedItemsQty() {
        shoppingCartPage.getAddedItems().get(0).waitUntil(Condition.visible, 10000);
        log.info("There is(are) " + shoppingCartPage.getAddedItems().shouldBe(CollectionCondition.sizeGreaterThan(0)).size() + " items in the shopping cart");
        return shoppingCartPage.getAddedItems().shouldBe(CollectionCondition.sizeGreaterThan(0)).size();
    }

    public ShoppingCartPageService clickOnTrashButton() {
        while (!shoppingCartPage.getAddedItemsTitlesCollection().isEmpty()) {
            log.info("There is(are) " + shoppingCartPage.getAddedItemsTitlesCollection().size() + " items in the shopping cart.");
            SelenideElement addedItem = shoppingCartPage.getAddedItems().first();
            String addedItemName = addedItem.$(shoppingCartPage.getAddedItemTitle()).text();
            SelenideElement trashButton = shoppingCartPage.getTrashButtonsInCarts().first();
            addedItem.hover();
            trashButton.hover().click();
            shoppingCartPage.getCartContainer().waitUntil(Condition.exist, 10000);
            if (checkProductIsDeleted(addedItemName)) {
                log.info("Product " + addedItemName + " is successfully deleted.");
            }
        }
        return this;
    }

    public boolean checkProductIsDeleted(String productTitle) {
        shoppingCartPage.getDeletedItemTitle().get(0).waitUntil(Condition.exist, 10000);
        log.info("There is(are) " + shoppingCartPage.getDeletedItems().size() + " deleted products in the cart.");
        for (SelenideElement deletedItem : shoppingCartPage.getDeletedItemTitle()) {
            try {
                if (deletedItem.text().contains(productTitle)) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    public ShoppingCartPageService reloadPage() {
        getWebDriver().navigate().refresh();
        return this;
    }

    public boolean checkThatItemsAreDeleted() {
        return shoppingCartPage.getCartIsEmpty().waitUntil(Condition.visible, 10000).text().contains("Ваша корзина пуста");
    }


}
