package state;

import com.codeborne.selenide.SelenideElement;

public class World {
    private static String currentUrl;
    private static SelenideElement currentFotoramaElement;
    private static int actualIndicatorResult;

    public static int getActualIndicatorResult() {
        return actualIndicatorResult;
    }

    public static void setActualIndicatorResult(int actualIndicatorResult) {
        World.actualIndicatorResult = actualIndicatorResult;
    }

    public static String getPreviouslySetUrl() {
        return currentUrl;
    }

    public void setCurrentUrl(String url) {
        currentUrl = url;
    }

    public static SelenideElement getPreviouslyFotoramaElement() {
        return currentFotoramaElement;
    }

    public void setCurrentFotoramaElement(SelenideElement element) {
        World.currentFotoramaElement = element;
    }
}
