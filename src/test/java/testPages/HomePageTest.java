package testPages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import pageObjects.HomePage;
import services.HomePageService;
import state.World;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;


public class HomePageTest {
    private HomePage page;
    private HomePageService homePageService = new HomePageService();

    World world = new World();
    String email = "test.ian87@gmail.com";
    String password = "LA147258369";
    String URL = "https://www.onliner.by/";
    String searchQuery = "iPhone 11";

    public HomePageTest() {
    }

    @Before
    public void initConfig() {
//        Configuration.remote = "http://localhost:4444/wd/hub";
        Configuration.timeout = 8000;
        Configuration.reportsFolder = "test-result/reports";
        Configuration.browser = "firefox";
        Configuration.browserSize = "1920x1080";
        Configuration.headless = true;
        BasicConfigurator.configure();
        open(URL);
        world.setCurrentUrl(url());

        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> WebDriverRunner.getWebDriver().quit())
        );
    }

    @Test
    public void test_Login() {
        homePageService
                .clickEnter()
                .setUserName(email)
                .setPassword(password)
                .clickLogin()
                .checkProfileImageAppears();
    }

    @Test
    public void test_Logout() {
        homePageService
                .login(email, password)
                .clickProfileImage()
                .clickLogout()
                .checkProfileImageDisappear()
                .checkUserStaysOnTheSamePage(World.getPreviouslySetUrl());
    }

    @Test
    public void test_Search_Results_Existence() {
        homePageService.clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .checkSearchResults(searchQuery);
    }

    @Test
    public void test_Search_Results_Match_Through_API() {
        homePageService.clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .checkSearchAPIResults(searchQuery);
    }

}
