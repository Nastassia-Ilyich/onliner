package testPages;

import com.automation.remarks.junit.VideoRule;
import com.automation.remarks.video.annotations.Video;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideDriver;
import com.codeborne.selenide.WebDriverRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import services.HomePageService;
import services.ProductDetailsPageService;
import services.ShoppingCartPageService;
import state.World;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.Assert.*;

public class ShoppingCartTest {

    @Rule
    public VideoRule videoRule = new VideoRule();

    private HomePageService homePageService = new HomePageService();
    private ShoppingCartPageService shoppingCartPageService = new ShoppingCartPageService();
    private ProductDetailsPageService pdpService = new ProductDetailsPageService();

    World world = new World();
    String URL = "https://www.onliner.by/";
    String searchQuery = "iPhone 11 Pro";

    @Before
    public void initConfig() {
//        Configuration.remote = "http://localhost:4444/wd/hub";
        Configuration.timeout = 8000;
        Configuration.headless = true;
        Configuration.reportsFolder = "test-result/reports";
        Configuration.browser = "chrome";
        BasicConfigurator.configure();
        open(URL);
        WebDriver driver = WebDriverRunner.getWebDriver();
        driver.manage().window().maximize();
        world.setCurrentUrl(url());

        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> WebDriverRunner.getWebDriver().quit())
        );
    }

    @Video
    @Test
    public void test_Added_Products_Exist_In_Cart() {
        homePageService
                .clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        pdpService
                .clickOnAddToButtonNearTheRatedShop()
                .goToTheShoppingCart();
        assertEquals(shoppingCartPageService.getAddedItemsQty(), World.getActualIndicatorResult());
    }

    @Test
    public void test_Deletion_Of_Product_That_Exist_In_Cart() {
        homePageService
                .clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        pdpService
                .clickOnAddToButtonNearTheRatedShop()
                .goToTheShoppingCart();
        shoppingCartPageService
                .clickOnTrashButton()
                .reloadPage()
                .checkThatItemsAreDeleted();
    }
}
