package testPages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideDriver;
import com.codeborne.selenide.WebDriverRunner;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import services.HomePageService;
import services.ProductDetailsPageService;
import state.World;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.Assert.assertTrue;

@Log4j
public class ProductDetailsTest {
    private HomePageService homePageService = new HomePageService();
    private ProductDetailsPageService productDetailsService = new ProductDetailsPageService();
    private SelenideDriver driver;

    World world = new World();
    String URL = "https://www.onliner.by/";
    String searchQuery = "iPhone 11 Pro";

    @Before
    public void initConfig() {
//        Configuration.remote = "http://localhost:4444/wd/hub";
        Configuration.timeout = 8000;
        Configuration.headless = true;
        Configuration.reportsFolder = "test-result/reports";
        Configuration.browser = "chrome";
        BasicConfigurator.configure();
        open(URL);
        WebDriver driver = WebDriverRunner.getWebDriver();
        driver.manage().window().maximize();
        world.setCurrentUrl(url());

        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> WebDriverRunner.getWebDriver().quit())
        );    }

    @Test
    public void test_User_Able_To_Get_To_The_PDP_Via_Quick_Search() {
        homePageService
                .clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        productDetailsService.checkUserIsOnNecessaryPDPPage(searchQuery);
    }

    @Test
    public void test_Existence_Of_Fotorama() {
        homePageService
                .clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        if (productDetailsService.getQtyOfElementsFromTheScrollBar() != 0) {
            log.info("Scroll bar has at least one element");
            productDetailsService.clickOnTheFirstThumbnailFromTheScrollBar();
        } else {
            log.error("Sorry, there are no elements in a scroll bar which related to this product");
        }
        productDetailsService.checkExistenceOfTheFotorama();
    }

    @Test
    public void test_Ability_To_Surf_Fotorama_Via_Thumbnails() {
        homePageService
                .clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        if (productDetailsService.getQtyOfElementsFromTheScrollBar() > 1) {
            log.info("Scroll bar has at least two elements");
        } else {
            log.error("Sorry, there are no necessary qty of elements (at least two) in a scroll bar which related to this product");
        }
        productDetailsService
                .clickOnTheFirstThumbnailFromTheScrollBar()
                .checkExistenceOfTheFotorama()
                .getCollectionOfThumbnailsElementsFromTheScrollBar().get(1).click();
        productDetailsService.checkThumbnailInFotoramaChangesOnTheSecond();
    }


    @Test
    public void test_Pointers_On_The_First_Element_In_Scroll_Bar() {
        homePageService
                .clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        if (productDetailsService.getQtyOfElementsFromTheScrollBar() > 1) {
            log.info("Scroll bar has at least two elements");
        } else {
            log.error("Sorry, there are no necessary qty of elements (at least two) in a scroll bar which related to this product");
        }
        productDetailsService.clickOnTheFirstThumbnailFromTheScrollBar();
        assertTrue(productDetailsService.checkExistenceOfNextPointer() && (!productDetailsService.checkExistenceOfBackPointer()));
    }

    @Test
    public void test_Pointers_On_The_Last_Element_In_Scroll_Bar() {
        homePageService.clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        if (productDetailsService.getQtyOfElementsFromTheScrollBar() > 1) {
            log.info("Scroll bar has at least two elements");
        } else {
            log.error("Sorry, there are no necessary qty of elements (at least two) in a scroll bar which related to this product");
        }
        productDetailsService.clicksOnTheLastThumbnail();
        assertTrue(!productDetailsService.checkExistenceOfNextPointer() && (productDetailsService.checkExistenceOfBackPointer()));
    }

    @Test
    public void test_Ability_To_Use_Pointers_To_Surf_Fotorama() {
        homePageService.clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        if (productDetailsService.getQtyOfElementsFromTheScrollBar() > 1) {
            log.info("Scroll bar has at least two elements");
        } else {
            log.error("Sorry, there are no necessary qty of elements (at least two) in a scroll bar which related to this product");
        }
        productDetailsService
                .clickOnTheFirstThumbnailFromTheScrollBar()
                .checkExistenceOfTheFotorama()
                .clickFotoramaNextPointer()
                .checkThumbnailInFotoramaChangesOnTheSecond()
                .clickFotoramaBackPointer()
                .checkThumbnailInFotoramaChangesOnTheLast();
    }

    @Test
    public void test_Buttons_For_Quick_Shopping_For_Rated_Shop() {
        homePageService
                .clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        productDetailsService
                .checkButtonsExistenceForTopRatedShop();
    }

    @Test
    public void test_Cart_Indicator_Is_Shown() {
        homePageService
                .clickOnSearchField()
                .enterSearchQuery(searchQuery)
                .clickOnFirstProductInSearchResults();
        productDetailsService
                .clickOnAddToButtonNearTheRatedShop()
                .checkIndicatorIsShown();
    }

}
