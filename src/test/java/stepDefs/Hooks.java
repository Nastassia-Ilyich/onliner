package stepDefs;

import com.codeborne.selenide.Configuration;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.plugin.ConcurrentEventListener;
import io.cucumber.plugin.event.*;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.BasicConfigurator;
import pageObjects.BasePage;
import state.World;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.url;

@Log4j
@ParametersAreNonnullByDefault
public class Hooks extends BasePage implements ConcurrentEventListener {
    private BasePage basePage;
    private World world = new World();

    public Hooks(BasePage base) {
        this.basePage = base;
    }

    public Hooks() {
    }

    @Before
    public void openPage() {
//        Configuration.remote = "http://localhost:4444/wd/hub";
        Configuration.timeout = 10000;
        Configuration.reportsFolder = "test-result/reports";
        Configuration.browser = "chrome";
        Configuration.headless = true;
//        Configuration.headless = false;
        BasicConfigurator.configure();
        open(basePage.baseURL);
        driver = getWebDriver();
        driver.manage().window().maximize();
        world.setCurrentUrl(url());

        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> {
                    driver.quit();
                    driver = null;
                })
        );
    }

    @Override
    public void setEventPublisher(EventPublisher eventPublisher) {
        eventPublisher.registerHandlerFor(TestRunStarted.class, setup);
        eventPublisher.registerHandlerFor(TestRunFinished.class, teardown);
        eventPublisher.registerHandlerFor(TestCaseStarted.class, testStart);
        eventPublisher.registerHandlerFor(TestCaseFinished.class, testFinish);
    }

    private EventHandler<TestRunStarted> setup = event -> beforeAll();

    private EventHandler<TestCaseFinished> testFinish = event -> testFinished();

    private EventHandler<TestCaseStarted> testStart = event -> testStarted();

    private EventHandler<TestRunFinished> teardown = event -> afterAll();

    private void afterAll() {
        log.info("Test run finished");

    }

    private void testStarted() {
        log.info("Test starts");
    }

    private void testFinished() {
        log.info("Test finished");
    }

    private void beforeAll() {
        log.info("Before tests run");
    }
}
