package stepDefs.pdp;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import services.HomePageService;
import services.ProductDetailsPageService;

public class ExistenceOfButtonsForTheTopShopStepdefs {
    private ProductDetailsPageService pdpPage = new ProductDetailsPageService();

    @Given("Necessary PDP is opened via Quick Search by {string}")
    public void necessaryPDPIsOpenedViaQuickSearchBySearchInput(String searchInput) {
        HomePageService homePage = new HomePageService();
        homePage.clickOnSearchField()
                .enterSearchQuery(searchInput)
                .clickOnFirstProductInSearchResults();
    }

    @Then("Two buttons are available for the top shop")
    public void twoButtonsAreAvailableForTheTopShop() {
        pdpPage.checkButtonsExistenceForTopRatedShop();
    }
}
