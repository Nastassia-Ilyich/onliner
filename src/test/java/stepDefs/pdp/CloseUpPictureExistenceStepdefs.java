package stepDefs.pdp;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j;
import services.HomePageService;
import services.ProductDetailsPageService;

@Log4j
public class CloseUpPictureExistenceStepdefs {
    private ProductDetailsPageService pdpPage = new ProductDetailsPageService();

    @Given("Product Details Page is opened by the {string} with at least one element in a scroll box")
    public void productDetailsPageIsOpenedByTheSearchInputWithAtLeastOneElementInAScrollBox(String searchInput) {
        HomePageService homePage = new HomePageService();
        homePage
                .clickOnSearchField()
                .enterSearchQuery(searchInput)
                .clickOnFirstProductInSearchResults();
        if (pdpPage.getQtyOfElementsFromTheScrollBar()!=0) {
            log.info("Scroll bar has at least one picture");
        } else {
            log.error("Sorry, there are no pictures in a scroll bar which related to this product");
        }
    }

    @When("User clicks on the first element")
    public void userClicksOnTheFirstElement() {
        pdpPage.clickOnTheFirstThumbnailFromTheScrollBar();
    }

    @Then("User see a fotorama of the clicked element")
    public void userSeeAFotoramaOfTheClickedElement() {
        pdpPage.checkExistenceOfTheFotorama();
    }

}
