package stepDefs.pdp;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import services.HomePageService;
import services.ProductDetailsPageService;

public class RedirectionToPDPViaQuickSearchStepdefs {
    private HomePageService homePage;
    private ProductDetailsPageService pdpPage = new ProductDetailsPageService();

    @Given("User got Search Results by {string}")
    public void userGotSearchResultsBySearchInput(String searchInput) {
        homePage = new HomePageService();
        homePage.clickOnSearchField()
                .enterSearchQuery(searchInput);
    }

    @And("User clicks on the product")
    public void userClicksOnTheProduct() {
        homePage.clickOnFirstProductInSearchResults();
    }

    @Then("User gets to the product details page by {string}")
    public void userGetsToTheProductDetailsPage(String searchInput) {
        pdpPage.checkUserIsOnNecessaryPDPPage(searchInput);
    }
}
