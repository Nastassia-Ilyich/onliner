package stepDefs.pdp;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import services.ShoppingCartPageService;
import static org.junit.Assert.assertTrue;

public class ItemDeletionStepdefs {
    private ShoppingCartPageService shoppingCartPage = new ShoppingCartPageService();

    @When("User clicks on a trash button near all items")
    public void userClicksOnATrashButtonNearAllItems() {
        shoppingCartPage.clickOnTrashButton();
    }

    @And("User reload the page")
    public void userReloadThePage() {
        shoppingCartPage.reloadPage();
    }

    @Then("User see that items from basket are deleted")
    public void userSeeThatItemIsDeleted() {
        assertTrue(shoppingCartPage.checkThatItemsAreDeleted());
    }

}
