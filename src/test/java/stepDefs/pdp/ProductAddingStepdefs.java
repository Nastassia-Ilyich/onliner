package stepDefs.pdp;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import services.HomePageService;
import services.ProductDetailsPageService;
import services.ShoppingCartPageService;
import state.World;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProductAddingStepdefs {
    private ProductDetailsPageService pdpPage = new ProductDetailsPageService();
    private ShoppingCartPageService shoppingCartPage = new ShoppingCartPageService();

    @Given("Necessary PDP is opened with the help of the Quick Search by {string}")
    public void necessaryPDPIsOpenedWithThwHelpOfTheQuickSearchBySearchInput(String searchInput) {
        HomePageService homePage = new HomePageService();
        homePage.clickOnSearchField()
                .enterSearchQuery(searchInput)
                .clickOnFirstProductInSearchResults();
    }

    @When("User clicks on add to cart button")
    public void userClicksOnAddToCartButton() {
        pdpPage.clickOnAddToButtonNearTheRatedShop();
    }

    @And("Go to the cart page")
    public void goToTheCartPage() {
        pdpPage.goToTheShoppingCart();
    }

    @Then("User see successfully added product")
    public void userSeesSuccessfullyAddedProduct() {
        assertEquals(World.getActualIndicatorResult(), shoppingCartPage.getAddedItemsQty());
    }

    @Then("Indicator on a basket shows necessary qty")
    public void indicatorOnABasketShows() {
        pdpPage.checkIndicatorIsShown();
    }
}
