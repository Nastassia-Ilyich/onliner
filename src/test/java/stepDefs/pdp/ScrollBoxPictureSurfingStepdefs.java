package stepDefs.pdp;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j;
import services.HomePageService;
import services.ProductDetailsPageService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Log4j
public class ScrollBoxPictureSurfingStepdefs {
    private ProductDetailsPageService pdpPage = new ProductDetailsPageService();

    @Given("Product Details Page is opened by the {string} with at least two elements in a scroll box")
    public void productDetailsPageIsOpenedByTheSearchInputWithAtLeastTwoElementsInAScrollBox(String searchInput) {
        HomePageService homePage = new HomePageService();
        homePage.clickOnSearchField()
                .enterSearchQuery(searchInput)
                .clickOnFirstProductInSearchResults();
        if (pdpPage.getQtyOfElementsFromTheScrollBar()>1) {
            log.info("Scroll bar has at least two elements");
        } else {
            log.error("Sorry, there are no necessary qty of elements (at least two) in a scroll bar which related to this product");
        }
    }

    @When("User clicks on the first thumbnail")
    public void userClicksOnTheFirstThumbnail() {
        pdpPage.clickOnTheFirstThumbnailFromTheScrollBar();
    }

    @Then("User see a fotorama of the clicked thumb")
    public void userSeeAFotoramaOfTheClickedThumb() {
        pdpPage.checkExistenceOfTheFotorama();
    }

    @When("User clicks on the second thumbnail from the scroll bar")
    public void userClicksOnTheSecondThumbnailFromTheScrollBar() {
        pdpPage.getCollectionOfThumbnailsElementsFromTheScrollBar().get(1).click();
    }

    @Then("Element in fotorama changes")
    public void elementInFotoramaChangesOnTheSecond() {
        pdpPage.checkThumbnailInFotoramaChangesOnTheSecond();
    }

    @Then("User see a fotorama of a just clicked thumb")
    public void userSeeAFotoramaOfAJustClickedThumb() {
        pdpPage.checkExistenceOfTheFotorama();
    }

    @And("Fotorama back pointer is not available")
    public void fotoramaBackPointerIsNotAvailable() {
      assertFalse(pdpPage.checkExistenceOfBackPointer());
    }

    @And("Fotorama next pointer is available")
    public void fotoramaNextPointerIsAvailable() {
     assertTrue(pdpPage.checkExistenceOfNextPointer());
    }

    @When("User clicks on fotorama next pointer")
    public void userClicksOnFotoramaNextPointer() {
        pdpPage.clickFotoramaNextPointer();
    }

    @Then("The next thumbnail from the scroll box is opened")
    public void theNextThumbnailFromTheScrollBoxIsOpened() {
        pdpPage.checkThumbnailInFotoramaChangesOnTheSecond();
    }

    @When("User clicks on the last thumbnail")
    public void userClicksOnTheLastThumbnail() {
        pdpPage.clicksOnTheLastThumbnail();
    }

    @And("Fotorama next pointer is not available")
    public void fotoramaNextPointerIsNotAvailable() {
        assertFalse(pdpPage.checkExistenceOfNextPointer());
    }

    @And("Fotorama back pointer available")
    public void fotoramaBackPointerAvailable() {
        assertTrue(pdpPage.checkExistenceOfBackPointer());
    }

    @When("User clicks on the fotorama back pointer")
    public void userClicksOnTheFotoramaBackPointer() {
        pdpPage.clickFotoramaBackPointer();
    }

    @Then("User is redirected back to the last thumbnail")
    public void userIsRedirectedBackToTheFirstPicture() {
        pdpPage.checkThumbnailInFotoramaChangesOnTheLast();
    }

}
