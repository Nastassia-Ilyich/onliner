package stepDefs.homepage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pageObjects.BasePage;
import services.HomePageService;

public class LoginStepdefs extends BasePage {

    private HomePageService homePage;

    @Given("User is on a HomePage")
    public void userOpensURL() {
        homePage = new HomePageService();
    }

    @And("User clicks on enter button")
    public void userClicksOn() {
        homePage.clickEnter();
    }

    @And("User enters Email as {string} and Password as {string}")
    public void userEntersEmailAsAndPasswordAs(String email, String password) {
        homePage
                .setUserName(email)
                .setPassword(password);
    }

    @And("Click on login button")
    public void clickOnLoginBtn() {
        homePage.clickLogin();
    }

    @Then("Profile image appears")
    public void profileImageAppears() {
        homePage.checkProfileImageAppears();
    }

}
