package stepDefs.homepage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.BasePage;
import services.HomePageService;
import state.World;

public class LogoutStepdefs extends BasePage {
    private HomePageService homePage;
    String email = "test.ian87@gmail.com";
    String password = "LA147258369";

    @Given("User is sign in")
    public void userIsSignIn() {
        homePage = new HomePageService();
        homePage.login(email, password);
    }

    @When("User clicks on a profile image")
    public void userClicksOnAProfileImage() {
        homePage.clickProfileImage();
    }

    @And("Click on logout link")
    public void clickOnLogoutLink() {
        homePage.clickLogout();
    }

    @Then("Profile image disappears")
    public void profileImageDisappears() {
        homePage.checkProfileImageDisappear();
    }

    @And("User stays on the same page")
    public void userStaysOnTheSamePage() {
        homePage.checkUserStaysOnTheSamePage(World.getPreviouslySetUrl());
    }
}
