package stepDefs.homepage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pageObjects.BasePage;
import services.HomePageService;
import state.World;

public class SearchResultsStepdefs extends BasePage {
    private HomePageService homePage;

    @Given("User is on a necessary HomePage")
    public void userOpensNecessaryURL() {
        homePage = new HomePageService();
        homePage.checkUserStaysOnTheSamePage(World.getPreviouslySetUrl());
    }

    @And("User clicks on search field")
    public void userClicksOnSearchField() {
        homePage.clickOnSearchField();
    }

    @And("User enters search query {string}")
    public void userEntersSearchQuerySearchInput(String searchInput) {
        homePage.enterSearchQuery(searchInput);
    }

    @Then("Appropriate search Results appears {string}")
    public void searchResultsAppears(String searchInput) {
        homePage.checkSearchResults(searchInput);
    }
}
