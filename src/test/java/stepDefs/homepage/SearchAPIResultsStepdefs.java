package stepDefs.homepage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import services.HomePageService;
import state.World;

public class SearchAPIResultsStepdefs {
    private HomePageService homePage;

    @Given("User is on a home page")
    public void userIsOnAHomePage() {
        homePage = new HomePageService();
        homePage.checkUserStaysOnTheSamePage(World.getPreviouslySetUrl());
    }

    @And("User enters any search query {string}")
    public void userEntersAnySearchQuerySearchInput(String searchInput) {
        homePage
                .clickOnSearchField()
                .enterSearchQuery(searchInput);
    }

    @Then("Appropriate search Results are received {string}")
    public void appropriateSearchResultsAreReceivedSearchInput(String searchInput) {
        homePage.checkSearchAPIResults(searchInput);
    }
}
