Feature: Search via the search field (API check)

  Scenario Outline: Search results come in an appropriate way
    Given User is on a home page
    And User enters any search query '<search input>'
    Then Appropriate search Results are received '<search input>'

    Examples:
      | search input |
      |iPhone 6s      |
      |iPhone 7    |
      |Samsung Galaxy S20|