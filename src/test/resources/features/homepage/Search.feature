Feature: Search via the search field

  Scenario Outline: Existence of search results
    Given User is on a necessary HomePage
    And User clicks on search field
    And User enters search query '<search input>'
    Then Appropriate search Results appears '<search input>'
    Examples:
      | search input |
      |iPhone 11 Pro|
      |iPhone 8    |
      |Samsung Galaxy S21|