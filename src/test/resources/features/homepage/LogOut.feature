Feature: Logout

  Scenario: Successful logout from any page
    Given User is sign in
    When User clicks on a profile image
    And Click on logout link
    Then Profile image disappears
    And User stays on the same page