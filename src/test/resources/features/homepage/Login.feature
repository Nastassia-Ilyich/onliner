Feature: Login

  Scenario Outline: Successful login with valid credentials
    Given User is on a HomePage
    And User clicks on enter button
    And User enters Email as '<email>' and Password as '<password>'
    And Click on login button
    Then Profile image appears

    Examples:
    | email | password |
    |test.ian87@gmail.com|LA147258369|
