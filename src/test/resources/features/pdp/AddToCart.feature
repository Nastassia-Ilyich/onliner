Feature: Add to cart

  Scenario Outline: Product is successfully added to cart
    Given Necessary PDP is opened with the help of the Quick Search by '<search input>'
    When User clicks on add to cart button
    And Go to the cart page
    Then User see successfully added product

    Examples:
      | search input  |
      | iPhone 12 Pro |
      | iPhone 8      |
      | iPhone X      |

  Scenario Outline: Indicator shows items quantity on the shopping cart icon
    Given Necessary PDP is opened with the help of the Quick Search by '<search input>'
    When User clicks on add to cart button
    Then Indicator on a basket shows necessary qty

    Examples:
      | search input       |
      | iPhone 11 pro      |
      | iPhone 7           |
      | Samsung Galaxy S21 |

