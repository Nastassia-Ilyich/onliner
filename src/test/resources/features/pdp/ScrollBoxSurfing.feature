Feature: User is able to look through the product photos on PDP

  Scenario Outline: User can surf fotorama via scroll box
    Given Product Details Page is opened by the '<search input>' with at least two elements in a scroll box
    When User clicks on the first thumbnail
    Then User see a fotorama of the clicked thumb
    When User clicks on the second thumbnail from the scroll bar
    Then Element in fotorama changes

    Examples:
      | search input       |
      | iPhone 11 Pro Max  |
      | iPhone 7           |
      | Samsung Galaxy S20 |


  Scenario Outline: There is no back pointer, but there is next pointer on the first element in a scroll bar
    Given Product Details Page is opened by the '<search input>' with at least two elements in a scroll box
    When User clicks on the first thumbnail
    Then User see a fotorama of a just clicked thumb
    And Fotorama back pointer is not available
    And Fotorama next pointer is available

    Examples:
      | search input       |
      | iPhone 11 Pro Max  |
      | iPhone 7           |
      | Samsung Galaxy S20 |

  Scenario Outline: There is no next pointer, but there is back pointer on the last element in a scroll bar
    Given Product Details Page is opened by the '<search input>' with at least two elements in a scroll box
    When User clicks on the last thumbnail
    Then User see a fotorama of a just clicked thumb
    And Fotorama next pointer is not available
    And Fotorama back pointer available

    Examples:
      | search input       |
      | iPhone 11 Pro Max  |
      | iPhone 7           |
      | Samsung Galaxy S20 |


  Scenario Outline: User is able to use pointers to see pictures from scroll box
    Given Product Details Page is opened by the '<search input>' with at least two elements in a scroll box
    When User clicks on the first thumbnail
    Then User see a fotorama of a just clicked thumb
    When User clicks on fotorama next pointer
    Then The next thumbnail from the scroll box is opened
    When User clicks on the fotorama back pointer
    Then User is redirected back to the last thumbnail

    Examples:
      | search input       |
      | iPhone 11 Pro Max  |
      | iPhone 7           |
      | Samsung Galaxy S20 |

