Feature: User is able to see desired close-up product element

  Scenario Outline: User see  close-up picture he clicked on
    Given Product Details Page is opened by the '<search input>' with at least one element in a scroll box
    When User clicks on the first element
    Then User see a fotorama of the clicked element

    Examples:
      | search input             |
      | iPhone 11 Pro Max        |
      | iPhone 7                 |
      | Samsung Galaxy S20       |
