Feature: Buttons for the top rated shop

  Scenario Outline: There two buttons for the more convenient shopping for the rated shop
    Given Necessary PDP is opened via Quick Search by '<search input>'
    Then Two buttons are available for the top shop

    Examples:
      | search input       |
      | iPhone 11 pro      |
      | iPhone 7           |
      | Samsung Galaxy S20 |