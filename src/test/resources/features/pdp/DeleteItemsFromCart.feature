Feature: Delete all from cart

  Scenario Outline: All products are successfully deleted from cart
    Given Necessary PDP is opened with the help of the Quick Search by '<search input>'
    When User clicks on add to cart button
    And Go to the cart page
    Then User see successfully added product
    When User clicks on a trash button near all items
    And User reload the page
    Then User see that items from basket are deleted

    Examples:
      | search input |
      | Xiaomi Redmi |
      | POCO X3      |