Feature: Necessary Product Details Page Open

  Scenario Outline: PDP page opens through the Search
    Given User got Search Results by '<search input>'
    And User clicks on the product
    Then User gets to the product details page by '<search input>'

    Examples:
      | search input       |
      | iPhone 11 pro      |
      | iPhone 7           |
      | Samsung Galaxy S20 |